# encore

`encore` is a command-line application built to parse JoyCon data.

## Specification

`encore` will print out `Ready` as soon as everything is loaded. `encore` will not accept
input until after `Ready` is sent. After that, connections or data can be requested.

An `encore`/user conversation is shown in the following:

```
>Ready
<Connections
>Connection:12 34 56 78 9A BC,JoyCon (L),Battery:42%,Color:00FFFF/000000 (Stock Neon Blue)
>Connection:DE F1 23 45 67 89,JoyCon (R),Battery:100%,Color:7289DA/212223 (Unknown)
<Subscribe:12 34 56 78 9A BC
>StateData:12 34 56 78 9A BC,<data in base64>
>StateData:12 34 56 78 9A BC,<data in base64>
>StateData:12 34 56 78 9A BC,<data in base64>
```

Messages from `encore` will be shown with `>` and messages from the user will be shown with
`<`. Real messages from `encore` are not prefixed with `>`, and you should not send
`encore` messages prefixed with `<`.

### Connections

How to request a JoyCon connection or control an existing connection.

#### Requesting to scan for new JoyCons

To request a scan of JoyCons, send `Scan` to `encore`. `encore` will return a list of
`Device`s, their MAC addresses, names, and whether they could or could not be a JoyCon.

```
<Scan
>Device:12 34 56 78 9A BC,JoyCon (L),Probable
>Device:DE F1 23 45 67 89,Mouse,Improbable
```

#### Connecting to a JoyCon

You can attempt connection to a device as a JoyCon by sending `Connect <MAC>`.

```
<Connect:12 34 56 78 9A BC
>Connection:12 34 56 78 9A BC,JoyCon (L),Battery:42%,Color:00FFFF/000000 (Stock Neon Blue)
```

#### Listing Connections

You can list all current connections by sending `Connections`.

```
<Connections
>Connection:12 34 56 78 9A BC,JoyCon (L),Battery:42%,Color:00FFFF/000000 (Stock Neon Blue)
>Connection:DE F1 23 45 67 89,JoyCon (R),Battery:100%,Color:7289DA/212223 (Unknown)
```

### Data

Data includes the JoyCon's SPI, as well as its button, joystick, and gyro states.

#### Requesting SPI

Due to the size of the entire SPI, you can only request one SPI at a time.

```
<Spi:12 34 56 78 9A BC
>SpiData:12 34 56 78 9A BC,<data in base64>
```

#### Requesting Data

You can request data from all `Connection`s or just a single one.

```
<State
>StateData:12 34 56 78 9A BC,<data in base64>
>StateData:DE F1 23 45 67 89,<data in base64>
<State:12 34 56 78 9A BC
>StateData:12 34 56 78 9A BC,<data in base64>
```

#### Subscribing to a JoyCon's Data

Subscribing to a `Connection`'s `StateData` will send you a `StateData` whenever the
`StateData` for that `Connection` changes.

```
<Subscribe:12 34 56 78 9A BC
>StateData:12 34 56 78 9A BC,<data in base64>
<[other commands]
>StateData:12 34 56 78 9A BC,<data in base64>
<[other commands]
>StateData:12 34 56 78 9A BC,<data in base64>
```